# walker

【高德LBS开源组件大赛】Android目的地雷达

[原文链接](https://my.oschina.net/lihao19850403/blog/300983)

这是2014年的老项目，基于高德地图做了一个指路的小功能。具体使用方法可参考下面文档中的“使用说明”（位于文档最后）：

[2014原项目说明文件](./Walkers/README.md)

原项目是在Eclipse环境下开发的，我对其进行了重制，相关代码位于对应文件夹：

### RemakeAndroidJava

- 迁移代码至Android Studio。
- 重新申请高德地图apikey，替换新版jar包。
- 整理代码，对应用中的字号大小进行调整。
- 增加新的欢迎页，在欢迎页中增加README页面。
- 在欢迎页中进行精确定位和摄像头权限申请。

权限申请功能基于EasyPermissions库实现，并封装成了组件permissions-utils，用于简化权限申请流程。使用方法：引入组件module->在AndroidManifest.xml中登记所需权限->在应用的Activity中使用一句话申请：

```
PermissionsUtils.getInstance().requestPermissions(
    this, // 当前Activity上下文。
    "权限申请提示语，用于告知权限申请目的。",
    new String[]{ // 列出待申请的权限。
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.CAMERA},
    ()->{ /* 权限申请成功后的回调内容。 */ });
```

以上代码申请了多个权限，组件自动处理这些权限的申请：

- 在申请权限之前，会以告警框的形式告知用户权限使用目的。
- 当所有权限都获得申请后，执行权限申请成功后的回调内容。
- 当部分或全部权限被拒绝，则不会执行任何操作，等待用户下次申请。
- 当有权限被永久拒绝时，组件会以弹框提示的形式引导用户进入应用管理页面，用户可以手动授权。