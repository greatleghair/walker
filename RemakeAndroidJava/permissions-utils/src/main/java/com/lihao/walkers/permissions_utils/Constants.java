package com.lihao.walkers.permissions_utils;

/**
 * 一些常量信息。
 * @author lihao
 */
public class Constants {

    /** 权限申请请求码。 */
    public static final int REQUEST_CODE = 1000;

    /** 申请权限时候的提示语。 */
    public static final String PERMISSIONS_TIP = "permissionsTip";

    /** 具体申请哪些权限。 */
    public static final String PERMISSIONS_CONTENT = "permissionsContent";
}
