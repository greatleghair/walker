package com.lihao.walkers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.Manifest;

import com.lihao.walkers.dialog.ReadMeDialog;
import com.lihao.walkers.fork.OriginActivity;
import com.lihao.walkers.permissions_utils.PermissionsUtils;
import com.lihao.walkers.surfaceview.SurfaceActivity;
import com.lihao.walkers.utils.Constants;

/**
 * 进入程序用的Acitivty，执行一些初始化操作。
 * @author lihao
 */
public class EnterActivity extends OriginActivity implements View.OnClickListener {

    /** 全局配置信息。 */
    private SharedPreferences mConfig = null;

    /** 显示屏的描述信息。 */
    private DisplayMetrics mMetrics = null;

    /** 标题图。 */
    private ImageView mLogo = null;

    /** 标题图动画。 */
    private AnimationSet mLogoAnim = null;

    /** 标题文字。 */
    private TextView mTitle = null;

    /** 标题文字动画。 */
    private AnimationSet mTitleAnim = null;

    /** 按钮组。 */
    private LinearLayout mBtnGroup = null;

    /** 按钮组动画。 */
    private AnimationSet mBtnGroupAnim = null;

    /** README按钮。 */
    private Button mBtnReadMe = null;

    /** 启动按钮。 */
    private Button mBtnBoot = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_layout);
        initConfigFile();
        initPage();
        playAnimAndGoNext();
    }

    //////////////////////////////////////////////////

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnReadMe) {
            Intent intent = new Intent(this, ReadMeDialog.class);
            intent.putExtra(Constants.SCREEN_WIDTH, getScreenWidth());
            intent.putExtra(Constants.SCREEN_HEIGHT, getScreenHeight());
            startActivity(intent);
        } else if (id == R.id.btnBoot) {
            PermissionsUtils.getInstance().requestPermissions(
                    this,
                    getString(R.string.permissions_tip),
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CAMERA},
                    this::toSurfaceView);
        }
    }

    /**
     * 初始化配置信息。
     */
    private void initConfigFile() {
        mConfig = getSharedPreferences(Constants.CONFIG_FILE_NAME, Context.MODE_PRIVATE);
        if (mConfig != null) {
            // 记录屏幕的尺寸到配置文件。
            if (mMetrics == null) {
                mMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
            }
            int screenWidth = mMetrics.widthPixels;
            int screenHeight = mMetrics.heightPixels;
            Editor editor = mConfig.edit();
            editor.putInt(Constants.SCREEN_WIDTH, screenWidth);
            editor.putInt(Constants.SCREEN_HEIGHT, screenHeight);
            editor.commit();
            // 记录屏幕尺寸到内存中。
            setScreenWidth(mConfig.getInt(Constants.SCREEN_WIDTH, screenWidth));
            setScreenHeight(mConfig.getInt(Constants.SCREEN_HEIGHT, screenHeight));
        }
    }

    /**
     * 初始化页面元素。
     */
    private void initPage() {
        // 标题及动画。
        mLogo = findViewById(R.id.ivEntryLogo);
        mLogo.getLayoutParams().width = getScreenWidth() / 2;
        mLogo.getLayoutParams().height = mLogo.getLayoutParams().width;
        mLogoAnim = generateAnim(0, 0, -100, 0, 0, 1, 500);
        mTitle = findViewById(R.id.tvEntryTitle);
        mTitleAnim = generateAnim(0, 0, 100, 0, 0, 1, 500);
        // 按钮及动画。
        mBtnGroup = findViewById(R.id.llEntryBtnGroup);
        mBtnGroupAnim = generateAnim(0, 0, 100, 0, 0, 1, 500);
        mBtnReadMe = findViewById(R.id.btnReadMe);
        mBtnReadMe.setOnClickListener(this);
        mBtnBoot = findViewById(R.id.btnBoot);
        mBtnBoot.setOnClickListener(this);
    }

    /**
     * 初始化首页上的动画。
     * @param fromXDelta X轴移动起始位移。
     * @param toXDelta X轴移动终点位移。
     * @param fromYDelta Y轴移动起始位移。
     * @param toYDelta Y轴移动终点位移。
     * @param fromAlpha 初始状态透明度。
     * @param toAlpha 终止状态透明度。
     * @param durationMillis 动画持续时长（毫秒）。
     * @return 动画集。
     */
    private AnimationSet generateAnim(float fromXDelta, float toXDelta, float fromYDelta, float toYDelta, float fromAlpha, float toAlpha, long durationMillis) {
        AnimationSet animSet = new AnimationSet(true);
        TranslateAnimation transAnim = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
        transAnim.setDuration(durationMillis);
        animSet.addAnimation(transAnim);
        AlphaAnimation alphaAnim = new AlphaAnimation(fromAlpha, toAlpha);
        alphaAnim.setDuration(durationMillis);
        animSet.addAnimation(alphaAnim);
        return animSet;
    }

    /**
     * 播放动画并进入下一页。
     */
    private void playAnimAndGoNext() {
        mLogo.startAnimation(mLogoAnim);
        mTitle.startAnimation(mTitleAnim);
        mTitleAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) { }
            @Override public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(() -> {
                    mBtnGroup.setVisibility(View.VISIBLE);
                    mBtnGroup.startAnimation(mBtnGroupAnim);
                    mBtnReadMe.setEnabled(true);
                    mBtnBoot.setEnabled(true);
                }, 500);
            }
        });
    }

    /**
     * 去主视图Activity。
     */
    private void toSurfaceView() {
        Intent intent = new Intent(this, SurfaceActivity.class);
        intent.putExtra(Constants.SCREEN_WIDTH, getScreenWidth());
        intent.putExtra(Constants.SCREEN_HEIGHT, getScreenHeight());
        startActivity(intent);
        finish();
    }
}
