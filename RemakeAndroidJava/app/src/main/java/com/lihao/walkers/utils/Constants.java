package com.lihao.walkers.utils;

import com.lihao.walkers.dialog.AroundSettingDialog;
import com.lihao.walkers.dialog.SearchPOISettingDialog;
import com.lihao.walkers.map.MapActivity;
import com.lihao.walkers.surfaceview.SurfaceActivity;

/**
 * 存储一些常量。
 * @author lihao
 */
public class Constants {

	/** 默认相机旋转的角度。 */
	public static final int DEFAULT_DEGREE = 90;

	/**
	 * 摄像头支持的分辨率和屏幕的分辨率有一定的差距，若采用摄像头最高分辨率预览，则在低配置机器上会有卡顿。<br />
	 * 此处限制了这个差距，当摄像头支持的分辨率中没有和屏幕分辨率完全一致的时候，用这个系数，以便选择合适的分辨率进行预览。
	 */
	public static final float RESOLUTION_DIFF_LIMIT = 0.1f;

	/** 两次按下back键的时间间隔。 */
	public static final long BACK_PADDING = 1000;

	/** 方向传感器发送消息的时间间隔。 */
	public static final long UPGRADE_TRACK_PADDING = 35;

	/** 主按钮宽度占比。 */
	public static final float MAIN_BUTTON_WIDTH = 0.15f;

	/** 线性地图宽度占比。 */
	public static final float LINE_MAP_WIDTH = 0.41f;

	/** 进行周边查找的时候，自身周围半径范围，单位：米。 */
	public static final int AROUND_CIRCLE_RADIUS = 5000;

	/** 地球半径，单位：米。 */
	public static final double EARTH_RADIUS = 6378137.0;

	/** 当前纬度。 */
	public static final String CURRENT_LAT = "currentLat";

	/** 当前经度。 */
	public static final String CURRENT_LON = "currentLon";

	/** 全局配置信息文件的名字。 */
	public static final String CONFIG_FILE_NAME = "config";

	/** 屏幕宽。 */
	public static final String SCREEN_WIDTH = "screenWidth";

	/** 屏幕高。 */
	public static final String SCREEN_HEIGHT = "screenHeight";

	/** 周边查询的查询条件字符串。 */
	public static final String AROUND_SEARCH_KEY_WORDS = "aroundSearchKeyWords";

	/** 关键字搜索的查询条件。 */
	public static final String SEARCH_POI_KEY_WORD = "searchPOIKeyWord";

	/** 关键字搜索的查询城市。 */
	public static final String SEARCH_POI_KEY_CITY = "searchPOIKeyCity";

	/** 默认位置bean的显示文字。 */
	public static final String DEFAULT_TARGET_TEXT = "目标";

	/** 相机视图获得的坐标点数据提示。 */
	public static final String RECEIVE_LOCATION_DATA = "receiveLocationData";

	/** 基准数字。 */
	private static final int BASE_NUMBER = 0;

	/** 给{@link SurfaceActivity}发送的命令：打开地图页面。 */
	public static final int TO_MAP = BASE_NUMBER + 1;

	/** {@link SurfaceActivity}接收{@link MapActivity}的返回值：获得了一些坐标点。 */
	public static final int RECEIVE_POINTS = BASE_NUMBER + 2;

	/** 回传的是单一地点。 */
	public static final int DATA_SINGLE = BASE_NUMBER + 3;

	/** 回传的是多个地点。 */
	public static final int DATA_MULTI = BASE_NUMBER + 4;

	/** {@link MapActivity}接收{@link AroundSettingDialog}的返回值：获得了周边搜索条件。 */
	public static final int RECEIVE_AROUND_SETTINGS = BASE_NUMBER + 5;

	/** {@link MapActivity}接收{@link SearchPOISettingDialog}的返回值：获得了POI搜索条件。 */
	public static final int RECEIVE_SEARCH_POI_SETTINGS = BASE_NUMBER + 6;

	/** 给{@link SurfaceActivity}发送的命令：更新地理位置view。 */
	public static final int UPGRADE_TARGET_VIEW = BASE_NUMBER + 7;

	/** 省市名称列表。 */
	public static final String[] CITY_NAMES = {
			"北京", "上海", "天津", "重庆", "石家庄", "唐山", "秦皇岛", "邯郸", "邢台",
			"保定", "张家口", "承德", "沧州", "廊坊", "衡水", "太原", "大同", "阳泉",
			"长治", "晋城", "朔州", "晋中", "运城", "忻州", "临汾", "吕梁", "呼和浩特",
			"包头", "乌海", "赤峰", "通辽", "鄂尔多斯", "呼伦贝尔", "巴彦淖尔", "乌兰察布",
			"兴安", "锡林郭勒", "阿拉善", "沈阳", "大连", "鞍山", "抚顺", "本溪", "丹东",
			"锦州", "营口", "阜新", "辽阳", "盘锦", "铁岭", "朝阳", "葫芦岛", "长春",
			"吉林", "四平", "辽源", "通化", "白山", "松原", "白城", "延边", "哈尔滨",
			"齐齐哈尔", "鸡西", "鹤岗", "双鸭山", "大庆", "伊春", "佳木斯", "七台河",
			"牡丹江", "黑河", "绥化", "大兴安岭", "南京", "无锡", "徐州", "常州", "苏州",
			"南通", "连云港", "淮安", "盐城", "扬州", "镇江", "泰州", "宿迁", "杭州", "宁波",
			"温州", "嘉兴", "湖州", "绍兴", "金华", "衢州", "舟山", "台州", "丽水", "合肥",
			"芜湖", "蚌埠", "淮南", "马鞍山", "淮北", "铜陵", "安庆", "黄山", "滁州", "阜阳",
			"宿州", "巢湖", "六安", "亳州", "池州", "宣城", "福州", "厦门", "莆田", "三明",
			"泉州", "漳州", "南平", "龙岩", "宁德", "南昌", "景德镇", "萍乡", "九江", "新余",
			"鹰潭", "赣州", "吉安", "宜春", "抚州", "上饶", "济南", "青岛", "淄博", "枣庄",
			"东营", "烟台", "潍坊", "威海", "济宁", "泰安", "日照", "莱芜", "临沂", "德州",
			"聊城", "滨州", "菏泽", "郑州", "开封", "洛阳", "平顶山", "焦作", "鹤壁", "新乡",
			"安阳", "濮阳", "许昌", "漯河", "三门峡", "南阳", "商丘", "信阳", "周口", "驻马店",
			"武汉", "黄石", "襄樊", "十堰", "荆州", "宜昌", "荆门", "鄂州", "孝感", "黄冈",
			"咸宁", "随州", "恩施", "长沙", "株洲", "湘潭", "衡阳", "邵阳", "岳阳", "常德",
			"张家界", "益阳", "郴州", "永州", "怀化", "娄底", "湘西", "广州", "深圳", "珠海",
			"汕头", "韶关", "佛山", "江门", "湛江", "茂名", "肇庆", "惠州", "梅州", "汕尾",
			"河源", "阳江", "清远", "东莞", "中山", "潮州", "揭阳", "云浮", "南宁", "柳州",
			"桂林", "梧州", "北海", "防城港", "钦州", "贵港", "玉林", "百色", "贺州", "河池",
			"来宾", "崇左", "海口", "三亚", "成都", "自贡", "攀枝花", "泸州", "德阳", "绵阳",
			"广元", "遂宁", "内江", "乐山", "南充", "宜宾", "广安", "达州", "眉山", "雅安",
			"巴中", "资阳", "阿坝", "甘孜", "凉山", "贵阳", "六盘水", "遵义", "安顺", "铜仁",
			"毕节", "黔西南", "黔东南", "黔南", "昆明", "曲靖", "玉溪", "保山", "昭通", "丽江",
			"普洱", "临沧", "文山", "红河", "西双版纳", "楚雄", "大理", "德宏", "怒江", "迪庆",
			"拉萨", "昌都", "山南", "日喀则", "那曲", "阿里", "林芝", "西安", "铜川", "宝鸡",
			"咸阳", "渭南", "延安", "汉中", "榆林", "安康", "商洛", "兰州", "嘉峪关", "金昌",
			"白银", "天水", "武威", "张掖", "平凉", "酒泉", "庆阳", "定西", "陇南", "临夏", "甘南",
			"西宁", "海东", "海北", "黄南", "海南", "果洛", "玉树", "海西", "银川", "石嘴山", "吴忠",
			"固原", "中卫", "乌鲁木齐", "克拉玛依", "吐鲁番", "哈密", "和田", "阿克苏", "喀什",
			"克孜勒苏柯尔克孜", "巴音郭楞蒙古", "昌吉", "博尔塔拉蒙古", "伊犁哈萨克", "塔城", "阿勒泰",
			"香港", "澳门", "台北", "高雄", "基隆", "台中", "台南", "新竹", "嘉义"
	};
}
