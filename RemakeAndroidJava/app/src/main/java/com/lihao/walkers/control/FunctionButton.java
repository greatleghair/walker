package com.lihao.walkers.control;

import android.content.Context;
import android.util.AttributeSet;

import com.lihao.walkers.fork.OriginButton;

/**
 * 地图视图中的功能按钮。
 * @author lihao
 */
public class FunctionButton extends OriginButton {

	public FunctionButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

}
