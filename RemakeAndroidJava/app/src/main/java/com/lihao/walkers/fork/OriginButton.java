package com.lihao.walkers.fork;

import android.content.Context;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;

/**
 * 基准按钮。
 * @author lihao
 */
public class OriginButton extends AppCompatButton {

	/** 检测这个按钮是否被点击过。 */
	private boolean mIsTouched = false;

	/**
	 * 判断这个按钮是否曾被点击过。
	 * @return 是否曾被点击过。
	 */
	public boolean isTouched() {
		return mIsTouched;
	}

	/**
	 * 设置按钮已被点击过。
	 * @param isTouched 按钮已被点击过。
	 */
	public void setIsTouched(boolean isTouched) {
		this.mIsTouched = isTouched;
	}

	public OriginButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

}
