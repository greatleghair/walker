package com.lihao.walkers.fork;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

/**
 * 给Activity添加的一些额外方法和属性。
 * @author lihao
 */
public abstract class OriginActivity extends Activity {

	/** 屏幕宽度。 */
	private int mScreenWidth = -1;

	/** 屏幕高度。 */
	private int mScreenHeight = -1;

	/**
	 * 获得屏幕宽度。
	 * @return 屏幕宽度。
	 */
	public int getScreenWidth() {
		return mScreenWidth;
	}

	/**
	 * 设置屏幕的宽度。
	 * @param screenWidth 待设置的屏幕宽度。
	 */
	public void setScreenWidth(int screenWidth) {
		this.mScreenWidth = screenWidth;
	}

	/**
	 * 获得屏幕高度。
	 * @return 屏幕高度。
	 */
	public int getScreenHeight() {
		return mScreenHeight;
	}

	/**
	 * 设置屏幕的高度。
	 * @param screenHeight 待设置的屏幕高度。
	 */
	public void setScreenHeight(int screenHeight) {
		this.mScreenHeight = screenHeight;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initWindowConfig();
	}

	@SuppressLint("SourceLockedOrientationActivity")
	@Override
	protected void onResume() {
		super.onResume();
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	/**
	 * 获得屏幕方向。
	 * @return 屏幕方向。
	 */
	public int getRotation(){
		return getWindowManager().getDefaultDisplay().getRotation();
	}

	/**
	 * 初始化窗口。<br />
	 * 需要在{@link android.app.Activity#setContentView}方法前进行调用。
	 */
	private void initWindowConfig() {
		Window window = getWindow();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); // 设置屏幕高亮。
	}
}
