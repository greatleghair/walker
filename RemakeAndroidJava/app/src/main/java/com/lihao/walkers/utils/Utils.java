package com.lihao.walkers.utils;

import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;

/**
 * 一些简单的工具方法。
 * @author lihao
 */
public class Utils {

	/** 自身对象。 */
	private static Utils mThis = null;

	private Utils(){

	}

	public static Utils getInstance(){
		if(mThis == null){
			mThis = new Utils();
		}
		return mThis;
	}

	/**
	 * 获得文字高度。
	 * @param paint 画笔。
	 * @return 文字高度。
	 */
	public float getTextHeight(Paint paint){
		FontMetrics fm = paint.getFontMetrics();
		return fm.descent - fm.ascent;
	}

	/**
	 * 计算两个经纬度之间的距离。
	 * @param startLat 开始的纬度。
	 * @param startLon 开始的经度。
	 * @param endLat 结束的纬度。
	 * @param endLon 结束的经度。
	 * @return 距离，精确到米。
	 */
	public double calculateDistance(double startLat, double startLon,
									double endLat, double endLon){
		// 计算纬度差值。
		double radStartLat = startLat * Math.PI / 180.0f;
		double radEndLat = endLat * Math.PI / 180.0f;
		double latDistance = radStartLat - radEndLat;
		// 计算经度差值。
		double lonDistance = (startLon - endLon) * Math.PI / 180.0f;
		// 计算距离。
		double distance = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDistance / 2), 2)
				+ Math.cos(radStartLat) * Math.cos(radEndLat)
				* Math.pow(Math.sin(lonDistance / 2), 2)));
		distance = distance * Constants.EARTH_RADIUS;
		distance = Math.round(distance * 10000) / 10000;
		return distance;
	}

	/**
	 * 计算两个经纬度之间的方向角。
	 * @param startLat 开始的纬度。
	 * @param startLon 开始的经度。
	 * @param targetLat 目标纬度。
	 * @param targetLon 目标经度。
	 * @return 两个经纬度之间的方向角。
	 */
	public double calculateAngle(double startLat, double startLon,
								 double targetLat, double targetLon) {
		double angle = 0;
		startLat = startLat * Math.PI / 180;
		startLon = startLon * Math.PI / 180;
		targetLat = targetLat * Math.PI / 180;
		targetLon = targetLon * Math.PI / 180;
		angle = Math.sin(startLat) * Math.sin(targetLat)
				+ Math.cos(startLat) * Math.cos(targetLat) * Math.cos(targetLon - startLon);
		angle = Math.sqrt(1 - angle * angle);
		angle = Math.cos(targetLat) * Math.sin(targetLon - startLon) / angle;
		angle = Math.asin(angle) * 180 / Math.PI;
		if(startLat < targetLat && startLon > targetLat){ // 目标在西北方。
			angle = angle + 360;
		} else if(startLat > targetLat){ // 目标在南方。
			angle = 180 - angle;
		}
		return angle;
	}
}
