package com.lihao.walkers.control;

import com.lihao.walkers.fork.OriginButton;

import android.content.Context;
import android.util.AttributeSet;

/**
 * 主功能按钮。
 * @author lihao
 */
public class MainButton extends OriginButton {
	
	public MainButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

}
