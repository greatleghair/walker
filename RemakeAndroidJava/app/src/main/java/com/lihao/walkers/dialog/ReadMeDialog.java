package com.lihao.walkers.dialog;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lihao.walkers.R;
import com.lihao.walkers.fork.OriginActivity;
import com.lihao.walkers.utils.Constants;

/**
 * 自述文件。
 * @author lihao
 */
public class ReadMeDialog extends OriginActivity implements OnClickListener {

    /** 对话框背景。 */
    private LinearLayout mBg = null;

    /** README内容。 */
    private TextView mTip = null;

    /** 退出按钮。 */
    private Button mConfirm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read_me_layout);
        receiveData();
        init();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnExit) {
            finish();
        }
    }

    /**
     * 接收数据。
     */
    private void receiveData(){
        Bundle bundle = getIntent().getExtras();
        setScreenWidth(bundle.getInt(Constants.SCREEN_WIDTH));
        setScreenHeight(bundle.getInt(Constants.SCREEN_HEIGHT));
    }

    /**
     * 初始化。
     */
    private void init() {
        mBg = findViewById(R.id.llReadMeBg);
        mBg.getLayoutParams().width = (int) (getScreenWidth() * 0.9f);
        mBg.getLayoutParams().height = (int) (getScreenHeight() * 0.67f);
        mTip = (TextView) findViewById(R.id.tvContent);
        mTip.setMovementMethod(new ScrollingMovementMethod());
        mConfirm = (Button) findViewById(R.id.btnExit);
        mConfirm.setOnClickListener(this);
    }
}
