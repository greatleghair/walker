# walker

【高德LBS开源组件大赛】Android目的地雷达

### 开源组件名称

目的地雷达

### 开源组件说明及使用场景

使用高德地图查找目的地，获得经纬度后，计算自身与这些位置之间的距离，并显示在屏幕上。适用于需要得到自身与目的地之间距离的直观信息的场景。

### 开源组件所使用的技术

1. 高德地图。
2. Camera预览。
3. 地理位置与距离的换算。
4. 方向传感器的使用。

### Git代码托管地址

[https://git.oschina.net/greatleghair/walker/tree/master/Walkers](https://git.oschina.net/greatleghair/walker/tree/master/Walkers)

### 开源组件截图以及安装二维码

![](https://static.oschina.net/uploads/space/2014/0814/080309_bhlz_1987008.png)
![](https://static.oschina.net/uploads/space/2014/0814/080652_xpVu_1987008.jpg)
![](https://static.oschina.net/uploads/space/2014/0814/080723_gv73_1987008.png)

注：其实第三张图片的背景是相机的预览框。

安装二维码如下：

![](https://static.oschina.net/uploads/space/2014/0814/103738_nKRG_1987008.png)

### 使用说明

请在确保网络畅通的情况下进行使用。

1. 打开APP，点击左下方蓝色小按钮，进入地图页面。
2. 在地图页面，点击左下方“周边”按钮，进行一些常规设施的搜索。
3. 或者点击右下方“搜索”按钮，进行自定义的搜索。
4. 搜索完成后，可以点击地图，指定跟踪某一个点。
5. 也可以不点击地图，默认跟踪所有目标，之后点击正下方“开始追踪”按钮进行追踪。
6. 在Camera预览框中将显示所有目的地与自己的距离，使用时请注意周边车辆。