package com.lihao.walkers.surfaceview;

import java.lang.ref.WeakReference;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.lihao.walkers.R;
import com.lihao.walkers.control.MainButton;
import com.lihao.walkers.fork.OriginActivity;
import com.lihao.walkers.map.DataPackage;
import com.lihao.walkers.map.MapActivity;
import com.lihao.walkers.utils.Constants;
import com.lihao.walkers.utils.Utils;

/**
 * 主Activity。
 * @author lihao
 */
public class SurfaceActivity extends OriginActivity implements OnClickListener, AMapLocationListener {

	/** 目标跟踪view。 */
	private TargetView mTargetView = null;
	
	/** 主要的可视控件。 */
	private SurfaceView mMainSurfaceView = null;
	
	/** 可视控件的控制器。 */
	private SurfaceHolder mHolder = null;
	
	/** 取景框的回调。 */
	private MainViewCallback mCallback = null;
	
	/** 功能按钮。 */
	private MainButton mMainButton = null;
	
	/** 上一次点击back键的时间。 */
	private long mLastBackTime = -1;
	
	/** 主按钮的进入动画。 */
	private TranslateAnimation mMainBtnAnim = null;
	
	/** 方向传感器。 */
	private SensorManager mSensorManager = null;
	
	/** 方向传感器监听。 */
	private TrackSensorListener mSensorEventListener = null;
	
	/** 位置改变代理。 */
	private LocationManagerProxy mAMapLocationManager = null;  
	
	/** 页面控制类Handler。  */
	private static SurfaceHandler mHandler = null;
	
	/**
	 * 获得页面控制类Handler。
	 * @return 页面控制类Handler。
	 */
	public static SurfaceHandler getHandler() {
		return mHandler;
	}

	/**
	 * 接收一些操作页面之类的消息。
	 * @author lihao
	 */
	private static class SurfaceHandler extends Handler {

		/** 与当前Activity之间的软引用。 */
		private WeakReference<SurfaceActivity> mActivity;
	    
	    public SurfaceHandler(SurfaceActivity activity) {
	    	mActivity = new WeakReference<SurfaceActivity>(activity);  
	    }  

	    @Override  
	    public void handleMessage(Message msg) {
	    	int info = msg.what;
	    	switch (info) {
			case Constants.UPGRADE_TARGET_VIEW:
				float selfAngle = (Float) msg.obj; // 获得手机相对于正北的偏移角度。
				mActivity.get().upgradeDrawTrack(selfAngle);
				break;
			default:
				break;
			}
	    }  
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.surface_layout);
		receiveConfigInfo();
		init();
	}
	
	@Override
	protected void onResume() {
		Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		mSensorManager.registerListener(mSensorEventListener, sensor,
				SensorManager.SENSOR_DELAY_GAME);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		mSensorManager.unregisterListener(mSensorEventListener);
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		mAMapLocationManager.removeUpdates(this);
		mAMapLocationManager.destroy();
		mAMapLocationManager = null;
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		boolean quitFlag = false;
		if(mLastBackTime == -1){
			mLastBackTime = new Date().getTime(); // 记录第一次按下back键的时间。
		} else {
			long currentTime = new Date().getTime(); // 记录当前按下back键的时间。
			if(Math.abs(currentTime - mLastBackTime) > Constants.BACK_PADDING){
				mLastBackTime = currentTime; // 间隔过大，不退出。
			} else {
				quitFlag = true;
			}
		}
		if(quitFlag){
			quit();
		} else {
			Toast.makeText(this, getString(R.string.quit_tip), Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case Constants.RECEIVE_POINTS:
			if(data != null){
				DataPackage locationData = (DataPackage) data.getSerializableExtra(
						Constants.RECEIVE_LOCATION_DATA);
				if(locationData != null){
					double currentLat = data.getExtras().getDouble(Constants.CURRENT_LAT);
					double currentLon = data.getExtras().getDouble(Constants.CURRENT_LON);
					startDrawTrack(currentLat, currentLon, locationData);
					mAMapLocationManager.requestLocationUpdates(LocationProviderProxy.AMapNetwork, 1000, 0, this);  
				}
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btnMainButton:
			Intent toMapIntent = new Intent(this, MapActivity.class);
			toMapIntent.putExtra(Constants.SCREEN_WIDTH, getScreenWidth());
			toMapIntent.putExtra(Constants.SCREEN_HEIGHT, getScreenHeight());
			startActivityForResult(toMapIntent, Constants.RECEIVE_POINTS);
			break;

		default:
			break;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onLocationChanged(AMapLocation aMapLocation) {
		double lat = aMapLocation.getLatitude();
		double lon = aMapLocation.getLongitude();
		mTargetView.initStartLocation(lat, lon);
		mTargetView.upgradeTracking();
	}
	
	/**
	 * 获得传过来的配置信息。
	 */
	private void receiveConfigInfo(){
		Bundle bundle = getIntent().getExtras();
		setScreenWidth(bundle.getInt(Constants.SCREEN_WIDTH));
		setScreenHeight(bundle.getInt(Constants.SCREEN_HEIGHT));
	}

	/**
	 * 初始化控件。
	 */
	private void init(){
		mHandler = new SurfaceHandler(this);
		initAnims();
		initMainView();
		initMainButton();
		initSensor();
		initLocationManager();
	}
	
	/**
	 * 初始化动画。
	 */
	private void initAnims(){
		int smallerSize = Math.min(getScreenWidth(), getScreenHeight());
		mMainBtnAnim = createAnim(-smallerSize * Constants.MAIN_BUTTON_WIDTH,
				smallerSize * Constants.MAIN_BUTTON_WIDTH);
	}
	
	/**
	 * 生成线性动画。
	 * @param startX 开始x位置。
	 * @param startY 开始y位置。
	 * @return 从x移动到y的动画。
	 */
	private TranslateAnimation createAnim(float startX, float startY){
		TranslateAnimation anim = new TranslateAnimation(startX, 0, startY, 0);
		anim.setStartOffset(1000);
		anim.setDuration(500);
		return anim;
	}
	
	/**
	 * 初始化主视图。
	 */
	private void initMainView(){
		// 取景框。
		mMainSurfaceView = (SurfaceView) findViewById(R.id.svMainSurface);
		mHolder = mMainSurfaceView.getHolder();
		mHolder.setFixedSize(getScreenWidth(), getScreenHeight());
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		mCallback = new MainViewCallback(this, getScreenWidth(), getScreenHeight(),
				mHolder, getRotation());
		mHolder.addCallback(mCallback);
		// 目标跟踪框。
		mTargetView = (TargetView) findViewById(R.id.stvTargetView);
		int smallerSize = Math.min(getScreenWidth(), getScreenHeight());
		int targetHeight = (int) (
				getScreenHeight() - smallerSize * Constants.MAIN_BUTTON_WIDTH);
		RelativeLayout.LayoutParams targetLp =
				(LayoutParams) mTargetView.getLayoutParams();
		targetLp.width = getScreenWidth();
		targetLp.height = targetHeight;
	}
	
	/**
	 * 初始化主按钮。
	 */
	private void initMainButton(){
		int smallerSize = Math.min(getScreenWidth(), getScreenHeight());
		mMainButton = (MainButton) findViewById(R.id.btnMainButton);
		RelativeLayout.LayoutParams mainBtnLp = (LayoutParams) mMainButton.getLayoutParams();
		mainBtnLp.width = (int) (smallerSize * Constants.MAIN_BUTTON_WIDTH);
		mainBtnLp.height = (int) (smallerSize * Constants.MAIN_BUTTON_WIDTH);
		mainBtnLp.alignWithParent = true;
		mMainButton.setOnClickListener(this);
		mMainButton.startAnimation(mMainBtnAnim);
		// 确定中部目标跟踪框的字号。
		float textSize = Utils.getInstance().getOptionalTextSize(
				mMainButton, Constants.DEFAULT_TARGET_TEXT,
				mMainButton.getLayoutParams().width,
				mMainButton.getLayoutParams().height);
		mTargetView.setTextSize(textSize);
	}
	
	/**
	 * 初始化传感器。
	 */
	private void initSensor(){
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorEventListener = new TrackSensorListener();
	}
	
	/**
	 * 初始化位置监听。
	 */
	@SuppressWarnings("deprecation")
	private void initLocationManager(){
        mAMapLocationManager = LocationManagerProxy.getInstance(this);  
	}
	
	/**
	 * 绘制跟踪图示。
	 * @param currentLat 当前的经度坐标。
	 * @param currentLon 当前的纬度坐标。
	 * @param locationData 采集到的地理位置坐标数据包。
	 */
	private void startDrawTrack(
			double currentLat, double currentLon, DataPackage locationData){
		// 初始化数据。
		mTargetView.initStartLocation(currentLat, currentLon);
		if(locationData.getDataType() == Constants.DATA_SINGLE){
			mTargetView.setData(locationData.getSingleLocation());
		} else {
			mTargetView.setData(locationData.getMultiLocation());
		}
		mTargetView.initTracking();
	}
	
	/**
	 * 更新跟踪view里面的内容。
	 * @param selfAngle 手机自身相对于正北的偏移角度。
	 */
	public void upgradeDrawTrack(float selfAngle){
		mTargetView.setSelfAngle(selfAngle);
		mTargetView.upgradeTracking();
	}
	
	/**
	 * 退出程序。
	 */
	private void quit(){
		finish();
	}
}
