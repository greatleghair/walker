package com.lihao.walkers.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.lihao.walkers.R;
import com.lihao.walkers.fork.OriginActivity;
import com.lihao.walkers.utils.Constants;
import com.lihao.walkers.utils.Utils;

/**
 * 设置周边搜索的一些限制条件。
 * @author lihao
 */
public class AroundSettingDialog extends OriginActivity implements OnClickListener {

	/** 当前纬度。 */
	private double mCurrentLat = -1;
	
	/** 当前经度。 */
	private double mCurrentLon = -1;
	
	/** 对话框标题。 */
	private TextView mTip = null;
	
	/** 选项容器。 */
	private RelativeLayout mOptionsLayout = null;
	
	/** 确认按钮。 */
	private Button mConfirm = null;
	
	/** 用来存储搜索结果字符串的缓冲区。 */
	private StringBuffer mResult = new StringBuffer();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.around_setting_layout);
		receiveData();
		init();
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.btnAroundConfirm:
			String keyWorlds = createKeyWords();
			if(!TextUtils.isEmpty(keyWorlds)){
				Intent intent = new Intent();
				intent.putExtra(Constants.CURRENT_LAT, mCurrentLat);
				intent.putExtra(Constants.CURRENT_LON, mCurrentLon);
				intent.putExtra(Constants.AROUND_SEARCH_KEY_WORDS, createKeyWords());
				setResult(Constants.RECEIVE_AROUND_SETTINGS, intent);
			}
			finish();
			break;
		default:
			break;
		}
	}
	
	/**
	 * 接收数据。
	 */
	private void receiveData(){
		Bundle bundle = getIntent().getExtras();
		setScreenWidth(bundle.getInt(Constants.SCREEN_WIDTH));
		setScreenHeight(bundle.getInt(Constants.SCREEN_HEIGHT));
		mCurrentLat = bundle.getDouble(Constants.CURRENT_LAT);
		mCurrentLon = bundle.getDouble(Constants.CURRENT_LON);
	}
	
	/**
	 * 初始化。
	 */
	private void init(){
		mTip = (TextView) findViewById(R.id.tvAroundSearchTip);
		mOptionsLayout = (RelativeLayout) findViewById(R.id.llOptionsLayout);
		mConfirm = (Button) findViewById(R.id.btnAroundConfirm);
		int smallerSize = Math.min(getScreenWidth(), getScreenHeight());
		int checkBoxIndex = 0;
		int checkboxCount = mOptionsLayout.getChildCount();
		float textSize = Utils.getInstance().getOptionalTextSize(
				mTip, getString(R.string.around_this_place),
				getScreenWidth() / 3.0f, smallerSize * Constants.MAIN_BUTTON_WIDTH * 0.67f);
		// 确定tip文字字号及内容。
		mTip.setTextSize(textSize);
		StringBuffer tipInfo = new StringBuffer();
		tipInfo.append(getString(R.string.around_this_place)).append(":\n")
		.append("\t").append(getString(R.string.lat)).append(":").append(mCurrentLat).append("\n")
		.append("\t").append(getString(R.string.lon)).append(":").append(mCurrentLon).append("\n")
		.append(getString(R.string.search_places_like)).append(":");
		mTip.setText(tipInfo.toString());
		// 确定每个选项的布局及字号。
		for (; checkBoxIndex < checkboxCount; checkBoxIndex++) {
			CheckBox oneCheckBox = (CheckBox) mOptionsLayout.getChildAt(checkBoxIndex);
			RelativeLayout.LayoutParams optLp = (LayoutParams) oneCheckBox.getLayoutParams();
			optLp.leftMargin = checkBoxIndex % 2 == 0 ? 0 : smallerSize >> 1; // 设置左边距。
			oneCheckBox.setTextSize(textSize);
		}
		// 确定按钮布局及字号。
		RelativeLayout.LayoutParams confirmLp = (LayoutParams) mConfirm.getLayoutParams();
		confirmLp.width = getScreenWidth() >> 2;
		confirmLp.height = (int) (smallerSize * Constants.MAIN_BUTTON_WIDTH);
		mConfirm.setTextSize(textSize);
		mConfirm.setOnClickListener(this);
	}
	
	/**
	 * 创建搜索的关键字字符串。
	 * @return 搜索的关键字字符串。
	 */
	private String createKeyWords(){
		mResult.delete(0, mResult.length());
		mResult.setLength(0);
		int checkBoxIndex = 0;
		int checkBoxCount = mOptionsLayout.getChildCount();
		for (; checkBoxIndex < checkBoxCount; checkBoxIndex++) {
			CheckBox oneOption = (CheckBox) mOptionsLayout.getChildAt(checkBoxIndex);
			if(oneOption.isChecked()){
				mResult.append(oneOption.getText()).append("|");
			}
		}
		if(mResult.length() > 0){
			mResult.deleteCharAt(mResult.length() - 1);
		}
		return mResult.toString();
	}
}
