package com.lihao.walkers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.lihao.walkers.fork.OriginActivity;
import com.lihao.walkers.surfaceview.SurfaceActivity;
import com.lihao.walkers.utils.Constants;
import com.lihao.walkers.utils.Utils;

/**
 * 进入程序用的Acitivty，执行一些初始化操作。
 * @author lihao
 */
public class EnterActivity extends OriginActivity {

	/** 显示屏的描述信息。 */
	private DisplayMetrics mMetrics = null;
	
	/** 全局配置信息。 */
	private SharedPreferences mConfig = null;
	
	/** 加载提示。 */
	private TextView mLoadingInfo = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_layout);
		initConfigFile();
		if(!checkInitStatus()){
			quit();
		} else {
			toSurfaceView();
		}
	}
	
	/**
	 * 初始化配置信息。
	 */
	private void initConfigFile(){
		mConfig = getSharedPreferences(Constants.CONFIG_FILE_NAME, Context.MODE_PRIVATE);
		if(mConfig != null){
			setScreenWidth(mConfig.getInt(Constants.SCREEN_WIDTH, markdownScreenWidth()));
			setScreenHeight(mConfig.getInt(Constants.SCREEN_HEIGHT, markdownScreenHeight()));
		}
		mLoadingInfo = (TextView) findViewById(R.id.tvEntryLoading);
		mLoadingInfo.setTextSize(Utils.getInstance().getOptionalTextSize(mLoadingInfo,
				getResources().getString(R.string.loading),
				getScreenWidth(), getScreenHeight() * 0.05f));
	}
	
	/**
	 * 记录屏幕的宽度到配置文件，并返回这个宽度。
	 * @return 宽度。
	 */
	private int markdownScreenWidth(){
		if(mMetrics == null){
			mMetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
		}
		int screenWidth = mMetrics.widthPixels;
		Editor editor = mConfig.edit();
		editor.putInt(Constants.SCREEN_WIDTH, screenWidth);
		editor.commit();
		return screenWidth;
	}
	
	/**
	 * 记录屏幕的高度到配置文件，并返回这个高度。
	 * @return 高度。
	 */
	private int markdownScreenHeight(){
		if(mMetrics == null){
			mMetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
		}
		int screenHeight = mMetrics.heightPixels;
		Editor editor = mConfig.edit();
		editor.putInt(Constants.SCREEN_HEIGHT, screenHeight);
		editor.commit();
		return screenHeight;
	}
	
	/**
	 * 判断初始化是否成功。
	 * @return 初始化情况。
	 */
	private boolean checkInitStatus(){
		boolean result = true; // 默认初始化是成功的。
		if(getScreenWidth() <= 0 || getScreenHeight() <= 0){
			result = false;
		}
		return result;
	}
	
	/**
	 * 去主视图Activity。
	 */
	private void toSurfaceView(){
		Intent intent = new Intent(this, SurfaceActivity.class);
		intent.putExtra(Constants.SCREEN_WIDTH, getScreenWidth());
		intent.putExtra(Constants.SCREEN_HEIGHT, getScreenHeight());
		startActivity(intent);
		finish();
	}
	
	/**
	 * 退出程序。
	 */
	private void quit(){
		
	}
}
