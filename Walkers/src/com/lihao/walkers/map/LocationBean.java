package com.lihao.walkers.map;

import java.io.Serializable;

import android.text.TextUtils;

import com.lihao.walkers.utils.Constants;

/**
 * 地理位置bean。
 * @author lihao
 */
public class LocationBean implements Serializable{

	private static final long serialVersionUID = 1L;

	/** 纬度。 */
	private double mLatitude = 0;
	
	/** 经度。 */
	private double mLongitude = 0;
	
	public double getLatitude() {
		return mLatitude;
	}

	public void setLatitude(double latitude) {
		this.mLatitude = latitude;
	}

	public double getLongitude() {
		return mLongitude;
	}

	public void setLongitude(double longitude) {
		this.mLongitude = longitude;
	}

	/** 目标的描述文字。 */
	public String name = Constants.DEFAULT_TARGET_TEXT;
	
	/**
	 * 地理位置bean的构造方法。
	 * @param lat 经度。
	 * @param lon 纬度。
	 * @param beanName 描述文字。
	 */
	public LocationBean(double lat, double lon, String beanName){
		mLatitude = lat;
		mLongitude = lon;
		name = TextUtils.isEmpty(beanName) ? name : beanName;
	}
}
