package com.lihao.walkers.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.lihao.walkers.utils.Constants;

/**
 * 用于向相机视图传递数据。
 * @author lihao
 */
public class DataPackage implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 默认回传单一地点。 */
	private int mDataType = Constants.DATA_SINGLE;
	
	/** 存储多个地点。 */
	private List<LocationBean> mMultiLocation = new ArrayList<LocationBean>();
	
	/** 存储单个地点。 */
	private LocationBean mSingleLocation = null;

	public int getDataType() {
		return mDataType;
	}

	public void setDataType(int dataType) {
		this.mDataType = dataType;
	}

	public List<LocationBean> getMultiLocation() {
		return mMultiLocation;
	}

	public void setMultiLocation(List<LocationBean> multiLocation) {
		mSingleLocation = null;
		this.mMultiLocation = multiLocation;
	}

	public LocationBean getSingleLocation() {
		return mSingleLocation;
	}

	public void setSingleLocation(LocationBean singleLocation) {
		mMultiLocation.clear();
		this.mSingleLocation = singleLocation;
	}
}
